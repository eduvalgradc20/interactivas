<?php
include("includes/bd.php");
include "mcript.php";
session_start();

if (isset($_POST['conectarse'])) {

    $contrasena = $_POST['contrasena'];
    $correo_electronico = $_POST['correo_electronico'];


  $user = $database->select("usuarios_tb", "*", ["correo_electronico" => $correo_electronico]);
  
  if (count($user) > 0) {

    if ($desencriptar($user[0]['contrasena'])===$contrasena) {
      $_SESSION['login_user'] = $correo_electronico;
      header("location: principal.php");
    } else {
      $error = "Error en la contraseña";
    }
  } else {
    $error = "Usuario invalido";
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inicio Session</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

    <link rel="stylesheet" href="CSS/style_regitro.css">

    <link rel="stylesheet" href="CSS/stylePrincipal.css">


</head>

<body>
 <header class="container-fluid">

        <nav class="navbar navbar-expand-lg ">

            <div class="container-fluid">

                <a class="navbar-brand mb-5" href="principal.php"><img class="img-size" src="imgs/identificador.png"
                        alt="Identificador"></a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse mb-4 mt-1 " id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto  mb-lg-0">
                        <li class="nav-item mb-4 mt-1">
                            <a class="nav-link active text-light" aria-current="page"
                                href="form-receta.php">Recetas</a>
                        </li>
                        <li class="nav-item mb-4 mt-1">
                            <a class="nav-link text-light" href="#">Tips</a>
                        </li>
                        <li class="nav-item dropdown mb-4 mt-1">
                            <a class="nav-link dropdown-toggle text-light" href="#" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                Categorias
                            </a>
                            <ul class="dropdown-menu ">
                                <li><a class="dropdown-item" href="#">Desayuno</a></li>
                                <li><a class="dropdown-item" href="#">Bebidas</a></li>
                                <li><a class="dropdown-item" href="#">Entradas</a></li>
                                <li><a class="dropdown-item" href="#">Almuerzo</a></li>
                                <li><a class="dropdown-item" href="#">Postres</a></li>
                                <li><a class="dropdown-item" href="#">Sopas</a></li>

                            </ul>
                        </li>
                        <li class="nav-item dropdown mb-4 mt-1">
                            <a class="nav-link dropdown-toggle text-light" href="#" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                Ocasiones
                            </a>
                            <ul class="dropdown-menu ">
                                <li><a class="dropdown-item" href="#">Todas</a></li>
                                <li><a class="dropdown-item" href="#">Cumpleaños</a></li>
                                <li><a class="dropdown-item" href="#">Día del padre</a></li>
                                <li><a class="dropdown-item" href="#">Día de la madre</a></li>
                                <li><a class="dropdown-item" href="#">Día del niño</a></li>
                                <li><a class="dropdown-item" href="#">Navidad</a></li>
                                <li><a class="dropdown-item" href="#">Semana Santa</a></li>
                                <li><a class="dropdown-item" href="#">Verano</a></li>

                            </ul>
                        </li>
                    </ul>


                    <form class="d-flex mb-4 mt-1" role="search">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit"><img class="img-fluid"
                                src="imgs/searchIcon.png" alt="search"></button>
                    </form>

                    <a href="inicio-session.php" class="mx-3 mb-4 mt-1">
                        <img src="imgs/usuario.png" alt="usurio">
                    </a>

                </div>
            </div>
        </nav>


    </header>





    

    <section class="seccion-perfil-receta ">
        <div class="perfil-receta-header ">
            <div class="perfil-receta-portada ">


                <br><br>

            </div>
        </div>
    </section>
    <section class="container-fluid mt-5">


        <section class="container row">


            <div class="col-md">

                <img class="img-fluid w-100 mt-2" src="CSS/img/vegetales.jpg" alt="vegetables">

            </div>


            <div class="col-md">

                <h1 class="titulo text-center">Iniciar sesion en <span class="titulo_negrita">Organic
                    Taste</span></h1>

                <form action="inicio-session.php" method="post">


                    <div class="mb-0">
                      <label for="exampleInputEmail1" class="form-label">Correo electronico</label>
                      <input type="text" class="form-control" name="correo_electronico" aria-describedby="emailHelp">
                      <div id="emailHelp" class="form-text">Su correo electronico no sera compartido con nadie </div>
                    </div>
                    <div class="mb-0">
                      <label for="exampleInputPassword1" class="form-label">Contraseña</label>
                      <input type="text" class="form-control" name="contrasena">
                    </div>
                   
                    <a name="conectarse" id="" class="btn btn-primary btn-danger mt-3 " 
                    role="button"> Conectarse</a>
                  </form>

                  <p class="">Olvidaste tu contrseña? <a name="password" type="button" class="btn btn-link color_verde" href="recuperar.php"
                    role="button">Recuperar contraseña</a>
                  </p>
  
  
                  <p class="">No tienes una cuena? <a name="registrarse" type="button" class="btn btn-link color_verde" href="registrarse.php"
                    role="button">Registrarse</a></p>

            </div>


        </section>


    </section>






    </Section>


    <!--Footer-->

    <section class="container-fluid footer mt-5">

        <section class="footer row">
            <div class="container col">

                <img class="img-size w-25 m-4" src="imgs/identificador.png" alt="logo">
            </div>
            <div class="col-4 p-5">

                <a href="#"><img class="mx-2" src="imgs/instagram.png" alt="instagram"></a>
                <a href="#"><img class="mx-2" src="imgs/facebook.png" alt="facebook"></a>
                <a href="#"><img class="mx-2" src="imgs/pinterest.png" alt="pinterest"></a>

            </div>




        </section>

    </section>







    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js"
    integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk"
    crossorigin="anonymous"></script>



</body>



</html>