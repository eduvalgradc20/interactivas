<?php

include("includes/bd.php");


if(isset($_POST['guardar_usuario'])){

    $nombre= $_POST['nombre'];
    $apellido= $_POST['apellido'];
    $correo_electronico= $_POST['correo_electronico'];
    $contrasena= $encriptar($_POST['contrasena']);
    $confirmar_contrasena= $encriptar($_POST['confirmar_contrasena']);
    $perfil= $_POST['perfil'];
    $imagen = $_FILES['imagen']['name'];
    $temp_p =  $_FILES['imagen']['tmp_name'];

    move_uploaded_file($temp_p, 'imgs/'.$imagen);

    $resultado=$database->insert("usuarios_tb",[
        "correo_electronico"=>$correo_electronico,
        "nombre"=>$nombre,
        "apellido"=>$apellido,
        "contrasena"=>$contrasena,
        "confirmar_contrasena"=>$confirmar_contrasena,
        "perfil"=>$perfil,
        "imagen" => 'imgs/'.$imagen
    ]);

   
        

}

include("template/cabecera.php");


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>

    <style>
        a{
            text-decoration: none;
            color: white;
        }
    </style>
</head>
<body>
    <div class="container">

    <h1 class="text-center m-5">Usuario agregado correctamente</h1>
    
    <div class="container">
    <button class="btn btn-success align-center ml-5"><a href="perfil-usuario" >Ir a mi perfil</a></button>


    </div>


    </div>
   


</body>
</html>




<?php

include("template/pie.php");
?>