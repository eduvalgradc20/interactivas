<?php


include_once("template/cabecera.php");
?>

<?php

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $result = $database->select("usuario_tb", "*", ["id" => $id]);

    if (count($result) == 1) {
        $nombre = $result[0]['nombre'];
        $apellido = $result[0]['apellido'];
        $perfil = $result[0]['perfil'];
        $contrasena = $result[0]['contrasena'];
        $correo_electronico = $result[0]['correo_electronico'];
        $confirmar_contrasena = $result[0]['confirmar_contrasena'];
    }
}
?>
<?php

if (isset($_POST['actualizar'])) {

    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $correo_electronico = $_POST['correo_electronico'];
    $contrasena = $encriptar($_POST['contrasena']);
    $confirmar_contrasena = $encriptar($_POST['confirmar_contrasena']);
    $perfil = $_POST['perfil'];





    $result = $database->update("usuario_tb", [
        "correo_electronico" => $correo_electronico,
        "nombre" => $nombre,
        "apellido" => $apellido,
        "contrasena" => $contrasena,
        "confirmar_contrasena" => $confirmar_contrasena,
        "perfil" => $perfil
    ], [
        "id" => $id
    ]);

    if (!$result) {
        die("Query failed");
    }



    header("Location: principal.php");
}
?>




<section class="container-fluid mt-5">


    <section class="container row">


        <div class="col-md">

            <img class="img-fluid w-100 mt-4" src="CSS/img/vegetales.jpg" alt="vegetables">

        </div>


        <div class="col-md">

            <h1 class="titulo text-center">Registro <span class="titulo_negrita">Organic
                    Taste</span></h1>

            <form action="editar_usuario.php?id=<?php echo $_GET['id'] ?>" method="post">



                <select class="form-select form-select-sm mt-4" name="perfil" aria-label=".form-select-sm example">

                    <option value="cliente">Cliente</option>
                    <option value="administrador">Administrador</option>

                </select>



                <div class="mb-0">
                    <select name="perfil" value="<?php echo $perfil ?>" id="perfil" class="form-control" aria-label="Seleccione un perfil">
                        <?php if ($perfil == "Administrador") { ?>
                            <option selected value="Administrador">Administrador</option>
                            <option value="cliente">Cliente</option>

                        <?php } else if ($perfil == "cliente") { ?>
                            <option selected value="Cliente">Cliente</option>
                            <option value="Administrador">Administrador</option>

                        <?php   }  ?>
                    </select>

                </div>


                <div class="mb-0">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" name="nombre" value="<?php echo $nombre ?>" class="form-control" placeholder="Nombre" required>

                </div>

                <div class="mb-0">
                    <label for="apellido" class="form-label">Apellido</label>
                    <input type="text" name="apellido" value="<?php echo $apellido ?>" class="form-control" placeholder="Apellido" required>

                </div>


                <div class="mb-0">
                    <label for="correo" class="form-label">Correo electronico</label>
                    <input type="text" name="correo_electronico" value="<?php echo $correo_electronico ?>" class="form-control" placeholder="Correo" required>

                </div>


                <div class="mb-0">
                    <label for="contraseña" class="form-label">Contraseña</label>
                    <input type="text" name="contrasena" value="<?php echo $contrasena ?>" class="form-control" placeholder="contrasena" required>

                </div>

                <div class="mb-0">
                    <label for="contraseña" class="form-label">Confirmar contraseña</label>
                    <input type="text" name="confirmar_contrasena" value="<?php echo $confirmar_contrasena ?>" class="form-control" placeholder="confirmar contrasena" required>

                </div>

                <input type="submit" value="Actualizar usuario" class="btn btn-success btn-block" name="actualizar">
                <input type="button" class="btn btn-success btn-block" onclick="history.back()" name="Atras" value="Atrás">

            </form>


</div>
</section>
</section>