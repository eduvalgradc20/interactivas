<?php
include_once "template/cabecera.php";
?>

    <header class="container-fluid">

        <nav class="navbar navbar-expand-lg">

            <div class="container-fluid">

                <a class="navbar-brand" href="principal.html"><img class="img-size" src="imgs/identificador.png"
                        alt="Identificador"></a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active text-light" aria-current="page"
                                href="form-receta.html">Recetas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-light" href="#">Tips</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-light" href="#" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                Categorias
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Desayuno</a></li>
                                <li><a class="dropdown-item" href="#">Bebidas</a></li>
                                <li><a class="dropdown-item" href="#">Entradas</a></li>
                                <li><a class="dropdown-item" href="#">Almuerzo</a></li>
                                <li><a class="dropdown-item" href="#">Postres</a></li>
                                <li><a class="dropdown-item" href="#">Sopas</a></li>

                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-light" href="#" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                Ocasiones
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Todas</a></li>
                                <li><a class="dropdown-item" href="#">Cumpleaños</a></li>
                                <li><a class="dropdown-item" href="#">Día del padre</a></li>
                                <li><a class="dropdown-item" href="#">Día de la madre</a></li>
                                <li><a class="dropdown-item" href="#">Día del niño</a></li>
                                <li><a class="dropdown-item" href="#">Navidad</a></li>
                                <li><a class="dropdown-item" href="#">Semana Santa</a></li>
                                <li><a class="dropdown-item" href="#">Verano</a></li>

                            </ul>
                        </li>
                    </ul>


                    <form class="d-flex" role="search">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit"><img class="img-fluid"
                                src="imgs/searchIcon.png" alt="search"></button>
                    </form>

                    <a href="inicio-session.html" class="mx-3">
                        <img src="imgs/usuario.png" alt="usurio">
                    </a>

                </div>
            </div>
        </nav>


    </header>

    <section class="seccion-perfil-receta">
        <div class="perfil-receta-header">
            <div class="perfil-receta-portada">
                <!--Slider principal-->


                <section class="container p-0 mt-5  ">


                    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
                                class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="imgs/slider1.png" class="d-block w-100" alt="slider">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5 class="fs-1 fw-bold">Pizza Chilera</h5>
                                    <p>Pizza vegana con chiles verdes y rojos</p>
                                </div>
                            </div>


                            <div class="carousel-item">
                                <img src="imgs/slider2.jpg" class="d-block w-100 " alt="slider">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5 class="fs-1 fw-bold">Hamburguesa con carne vegana</h5>
                                    <p>Hamburguesa con lechuga, tomate, cebolla y carne de soja</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="imgs/slider3.jpg" class="d-block w-100" alt="slider">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5 class="fs-1 fw-bold">Hamburguesa Vegetariana</h5>
                                    <p>Hamburguesa con sabores distintos y nuevos</p>
                                </div>
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>



                </section>

            </div>
        </div>
    </section>


    </section>



    <!--Primera sección cards-->

    <section class="container mt-5">

        <h2>Recomendadas del día</h2>

        <section class="container-fluid cards-wrapper justify-content-center mt-5 row">

            <div id="carouselExampleSlidesOnly" class="carousel slide col-4" data-bs-ride="carousel">
                <div class="carousel-inner row">

                    <div class="carousel-item active">


                        <div class="card">

                            <div class="img-wrapper">

                                <img src="imgs/card1.jpg" alt="card1">

                            </div>

                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Calificación: 4</p>
                                <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                    role="button"> ver mas</a>
                            </div>
                        </div>



                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card2.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card3.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


            <div id="carouselExampleSlidesOnly" class="carousel slide col-4" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card4.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card5.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card6.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div id="carouselExampleSlidesOnly" class="carousel slide col-4" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card7.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card8.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card9.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


        </section>



    </section>


    <!--Segunda sección cards-->

    <section class="container mt-5">

        <h2>Platillos con mejor calificación</h2>

        <section class="container-fluid cards-wrapper justify-content-center mt-5 row">

            <div id="carouselExampleSlidesOnly" class="carousel slide col-4" data-bs-ride="carousel">
                <div class="carousel-inner row">

                    <div class="carousel-item active">


                        <div class="card">

                            <div class="img-wrapper">

                                <img src="imgs/card1.jpg" alt="card1">

                            </div>

                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">Calificación: 4</p>
                                <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                    role="button"> ver mas</a>
                            </div>
                        </div>



                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card2.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card3.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


            <div id="carouselExampleSlidesOnly" class="carousel slide col-4" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card4.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card5.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card6.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div id="carouselExampleSlidesOnly" class="carousel slide col-4" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card7.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card8.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="carousel-item">

                        <div class="cards-wrapper">

                            <div class="card">

                                <div class="img-wrapper">

                                    <img src="imgs/card9.jpg" alt="card1">

                                </div>

                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Calificación: 4</p>
                                    <a name="" id="" class="btn btn-primary btn-danger " href="detalle-receta.html"
                                        role="button"> ver mas</a>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


        </section>

    </section>



    <!--Footer-->
    <?php
include_once "template/pie.php";
?>