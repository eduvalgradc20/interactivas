<?php
include_once "template/cabecera.php";
?>


    <header class="container-fluid">

        <nav class="navbar navbar-expand-lg ">

            <div class="container-fluid">

                <a class="navbar-brand mb-5" href="principal.html"><img class="img-size" src="imgs/identificador.png"
                        alt="Identificador"></a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse mb-4 mt-1 " id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto  mb-lg-0">
                        <li class="nav-item mb-4 mt-1">
                            <a class="nav-link active text-light" aria-current="page"
                                href="form-receta.html">Recetas</a>
                        </li>
                        <li class="nav-item mb-4 mt-1">
                            <a class="nav-link text-light" href="#">Tips</a>
                        </li>
                        <li class="nav-item dropdown mb-4 mt-1">
                            <a class="nav-link dropdown-toggle text-light" href="#" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                Categorias
                            </a>
                            <ul class="dropdown-menu ">
                                <li><a class="dropdown-item" href="#">Desayuno</a></li>
                                <li><a class="dropdown-item" href="#">Bebidas</a></li>
                                <li><a class="dropdown-item" href="#">Entradas</a></li>
                                <li><a class="dropdown-item" href="#">Almuerzo</a></li>
                                <li><a class="dropdown-item" href="#">Postres</a></li>
                                <li><a class="dropdown-item" href="#">Sopas</a></li>

                            </ul>
                        </li>
                        <li class="nav-item dropdown mb-4 mt-1">
                            <a class="nav-link dropdown-toggle text-light" href="#" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                Ocasiones
                            </a>
                            <ul class="dropdown-menu ">
                                <li><a class="dropdown-item" href="#">Todas</a></li>
                                <li><a class="dropdown-item" href="#">Cumpleaños</a></li>
                                <li><a class="dropdown-item" href="#">Día del padre</a></li>
                                <li><a class="dropdown-item" href="#">Día de la madre</a></li>
                                <li><a class="dropdown-item" href="#">Día del niño</a></li>
                                <li><a class="dropdown-item" href="#">Navidad</a></li>
                                <li><a class="dropdown-item" href="#">Semana Santa</a></li>
                                <li><a class="dropdown-item" href="#">Verano</a></li>

                            </ul>
                        </li>
                    </ul>


                    <form class="d-flex mb-4 mt-1" role="search">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit"><img class="img-fluid"
                                src="imgs/searchIcon.png" alt="search"></button>
                    </form>

                    <a href="inicio-session.html" class="mx-3 mb-4 mt-1">
                        <img src="imgs/usuario.png" alt="usurio">
                    </a>

                </div>
            </div>
        </nav>


    </header>
    <section class="seccion-perfil-receta ">
        <div class="perfil-receta-header ">
            <div class="perfil-receta-portada ">


            <br><br>

            </div>
        </div>
    </section>

    <section class="container-fluid mt-5">


        <section class="container row gap-5">


            <div class="col-md">

                <img class="img-fluid w-100 mt-2" src="CSS/img/vegetales.jpg" alt="vegetables">

            </div>


            <div class="col-md">

                <h1 class="titulo text-center">Recuperar contraseña <span class="titulo_negrita"> Organic
                    Taste</span></h1>

                <form>
                    <div class="mt-5 ">
                      <label for="exampleInputEmail1" class="form-label">Digite el correo electronico que utiliza para su cuenta</label>
                      <input type="email" class="form-control" id="email" aria-describedby="emailHelp">
                      <div id="emailHelp" class="form-text">A este correo se le enviaran lospasos a seguir para cambiar su contraseña </div>
                    </div>
                  
                    <a name="" id="" class="btn btn-primary btn-danger mt-3 " href="inicio-session.html"
                    role="button"> Enviar</a>
                    
                  </form>

                  
                  </p>
  
  
           

            </div>


        </section>


    </section>






    </Section>


    <?php
include_once "template/pie.php";
?>